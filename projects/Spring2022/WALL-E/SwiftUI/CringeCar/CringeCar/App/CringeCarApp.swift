//
//  CringeCarApp.swift
//  CringeCar
//
//  Created by dokerplp on 3/10/22.
//

import SwiftUI

@main
struct CringeCarApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
